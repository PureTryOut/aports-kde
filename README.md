Note that not all packages build fully with Qt6/KF6 yet.
A detailed list can be found at https://community.kde.org/KDE_Gear/24.02_Release_notes#Misc.
Because of this some packages are still built with Qt5 instead of Qt6 for now, and some use git master rather than the KDE6 release.
