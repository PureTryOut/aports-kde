# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=akonadi-contacts
pkgver=24.01.95
pkgrel=0
pkgdesc="Libraries and daemons to implement Contact Management in Akonadi"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by akonadi
# ppc64le blocked by qt6-qtwebengine -> akonadi
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later AND GPL-2.0-or-later AND BSD-3-Clause"
depends_dev="
	akonadi-dev
	gpgme-dev
	grantlee-dev
	grantleetheme-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kcontacts-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kmime-dev
	kservice-dev
	ktextaddons-dev
	ktexttemplate-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkleo-dev
	prison-dev
	qt6-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/pim/akonadi-contacts.git"
source="https://download.kde.org/unstable/release-service/$pkgver/src/akonadi-contacts-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
f9e7579a436d522f9b67deee199628a36da918bed2b426f9272c6b9ba4a890d296794db54c019c7b0d39ca22d8f4e8d9b13e3e850b7a9c69802a892a0fb4c18a  akonadi-contacts-24.01.95.tar.xz
"
