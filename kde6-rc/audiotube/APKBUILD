# Contributor: Luca Weiss <luca@z3ntu.xyz>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=audiotube
pkgver=24.01.95
pkgrel=0
pkgdesc="Client for YouTube Music"
url="https://invent.kde.org/plasma-mobile/audiotube"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by purpose -> qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
license="GPL-2.0-or-later"
depends="
	gst-plugins-bad
	gst-plugins-good
	kirigami-addons
	kirigami
	purpose
	py3-ytmusicapi
	qt6-qtbase-sqlite
	yt-dlp
	"
makedepends="
	extra-cmake-modules
	futuresql-dev
	kcrash-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami-dev
	py3-pybind11-dev
	python3-dev
	qcoro-dev
	qt6-qtmultimedia-dev
	qt6-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/multimedia/audiotube.git"
source="https://download.kde.org/unstable/release-service/$pkgver/src/audiotube-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a346f6b8b4a8721814b5ccbefd53510f53ab1eb8cc3eb8b151a880075c00ae1e0ca9e8307382add772af9e5e8639ff7a7c0271fa6b0b33fdab4cfa8c5fb05591  audiotube-24.01.95.tar.xz
"
