# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=k3b
pkgver=24.01.95
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/multimedia/org.kde.k3b"
pkgdesc="A full-featured CD/DVD/Blu-ray burning and ripping application"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="
	cdrdao
	dvd+rw-tools
	libburn
	"
makedepends="
	extra-cmake-modules
	flac-dev
	karchive-dev
	kcmutils-dev
	kconfig-dev
	kcoreaddons-dev
	kdoctools-dev
	kfilemetadata-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kservice-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	lame-dev
	libdvdread-dev
	libkcddb-dev
	libmad-dev
	libsamplerate-dev
	libvorbis-dev
	qt6-qtbase-dev
	samurai
	shared-mime-info
	solid-dev
	taglib-dev
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/multimedia/k3b.git"
source="https://download.kde.org/unstable/release-service/$pkgver/src/k3b-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DK3B_BUILD_MUSE_DECODER_PLUGIN=OFF \
		-DK3B_BUILD_SNDFILE_DECODER_PLUGIN=OFF \
		-DK3B_ENABLE_MUSICBRAINZ=OFF
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f5529fb0804aa38eb99bcea8695a4a4aa35ec44dc771795e99c2eb7a9573725a6e94f89027ef586332a7a3cfdde1072e8558e4819c0ce15289616bb6eecd9906  k3b-24.01.95.tar.xz
"
