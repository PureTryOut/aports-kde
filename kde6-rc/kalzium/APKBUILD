# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kalzium
pkgver=24.01.95
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://edu.kde.org/kalzium/"
pkgdesc="Periodic Table of Elements"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	eigen-dev
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kdoctools-dev
	ki18n-dev
	knewstuff-dev
	kparts-dev
	kplotting-dev
	kunitconversion-dev
	kwidgetsaddons-dev
	qt6-qtbase-dev
	qt6-qtscxml-dev
	qt6-qtsvg-dev
	samurai
	solid-dev
	"
_repo_url="https://invent.kde.org/education/kalzium.git"
source="https://download.kde.org/unstable/release-service/$pkgver/src/kalzium-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang $pkgname-dev"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2013d19d6a5ec275ee2641bd8a6c4158df9a759bc9811dfe90d16a94364d362a816e248431a9a070a61672ce314a74f5c56b42421ff5d14db8f2e9c641eb592e  kalzium-24.01.95.tar.xz
"
