# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kamera
pkgver=24.01.95
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/graphics"
pkgdesc="KDE integration for gphoto2 cameras"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kxmlgui-dev
	libgphoto2-dev
	qt6-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/graphics/kamera.git"
source="https://download.kde.org/unstable/release-service/$pkgver/src/kamera-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d3917be722f07969d86f63ab72418bac2325f442b4d5365e6853285d1b5488f58310e0eea330042fe1bf0f55d108dabd70286cbdc6ab3b2481e3fa96fb642699  kamera-24.01.95.tar.xz
"
