# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kblackbox
pkgver=24.01.95
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/org.kde.kblackbox"
pkgdesc="A game of hide and seek played on a grid of boxes"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	ktextwidgets-dev
	kxmlgui-dev
	libkdegames-dev
	qt6-qtbase-dev
	qt6-qtsvg-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/games/kblackbox.git"
source="https://download.kde.org/unstable/release-service/$pkgver/src/kblackbox-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
62f96c7e370426704a17f2f7bdf3b7fba39ecc5165f45dc1199ac73e572fe06874c2752937248cc1cd1fd394849c2355f2d3e88be696739bd2c839c69c0fca36  kblackbox-24.01.95.tar.xz
"
