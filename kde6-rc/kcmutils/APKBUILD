# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kcmutils
pkgver=5.249.0
pkgrel=0
pkgdesc="Utilities for interacting with KCModules"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="	
	kconfigwidgets-dev
	kcoreaddons-dev
	kdeclarative-dev
	ki18n-dev
	kiconthemes-dev
	kitemviews-dev
	kservice-dev
	kxmlgui-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt6-qttools-dev
	samurai
	"
checkdepends="
	kirigami
	xvfb-run
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/frameworks/kcmutils.git"
source="https://download.kde.org/unstable/frameworks/$pkgver/kcmutils-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure -E "kcmultidialogtest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8cb6ec3fd0fb0c650f0f567fe5a2cd9cd0ee15fd2cc6317fd0724bb54186fced6f74e5e86d54d013995dedc3a5173759a870b6d897f9cf16901f27cde7ccd141  kcmutils-5.249.0.tar.xz
"
