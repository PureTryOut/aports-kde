# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdebugsettings
pkgver=24.01.95
pkgrel=0
arch="all !armhf"
url="https://kde.org/applications/utilities/"
pkgdesc="An application to enable/disable qCDebug"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kitemviews-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt6-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/utilities/kdebugsettings.git"
source="https://download.kde.org/unstable/release-service/$pkgver/src/kdebugsettings-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3553738f67075f62948b1544f4f136e9ef1ba6cef800897bea5cd66646e21c381febcc9f903edaba04f459f3e1c3ad1897d16ebd80f7ffa54f9127defdbe42e9  kdebugsettings-24.01.95.tar.xz
"
