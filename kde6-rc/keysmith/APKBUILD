# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=keysmith
pkgver=24.01.95
pkgrel=0
pkgdesc="OTP client for Plasma Mobile and Desktop"
url="https://invent.kde.org/kde/keysmith"
arch="all !armhf"
license="GPL-3.0-or-later"
depends="kirigami"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	ki18n-dev
	kirigami-dev
	libsodium-dev
	qt6-qt5compat-dev
	qt6-qtbase-dev
	qt6-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/utilities/keysmith.git"
source="https://download.kde.org/unstable/release-service/$pkgver/src/keysmith-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
be380a9a2782ff9cab67e5c502bc736dd2394ddb7f23d8f74487cfb5e2fe3219fe2cee04e00417103fddcb9e33fb3bf0f1b78bd1579c645a9436392c9a1fea8b  keysmith-24.01.95.tar.xz
"
