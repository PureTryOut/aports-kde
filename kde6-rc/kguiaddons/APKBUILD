# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this apkbuild by some scripts for automation
# group=kde-frameworks
pkgname=kguiaddons
pkgver=5.249.0
pkgrel=0
pkgdesc="Addons to QtGui"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	plasma-wayland-protocols
	qt6-qttools-dev
	qt6-qtwayland-dev
	wayland-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	doxygen
	graphviz
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-doc $pkgname-bin"
_repo_url="https://invent.kde.org/frameworks/kguiaddons.git"
source="https://download.kde.org/unstable/frameworks/$pkgver/kguiaddons-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

bin() {
	amove usr/bin/kde-geo-uri-handler
	amove usr/share/applications
}

sha512sums="
da211721275ae5c7ff1e3f61938ea8a13991e122e5f942f7f1c6aea83cdb2920a4dd8119471e604f70211fac5fd563fcf14ce2cbe28539e5f6a9b330ceccd41e  kguiaddons-5.249.0.tar.xz
"
