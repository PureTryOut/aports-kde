# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kio-gdrive
pkgver=24.01.95
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt6-qtwebengine -> libkgapi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/KIO_GDrive"
pkgdesc="KIO Slave to access Google Drive"
license="GPL-2.0-or-later"
depends="
	kaccounts-providers
	signon-plugin-oauth2
	signon-ui
	"
makedepends="
	extra-cmake-modules
	kaccounts-integration-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	libkgapi-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/network/kio-gdrive.git"
source="https://download.kde.org/unstable/release-service//$pkgver/src/kio-gdrive-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
34783a64c987b470cc1d764ad0ac9e845889381875154479330ee8124f57127079d4760130850d9c5713b0dcf7487d579e975819a1af1dc1237205a2534f9f7f  kio-gdrive-24.01.95.tar.xz
"
