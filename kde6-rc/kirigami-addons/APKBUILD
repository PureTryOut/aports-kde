# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-other
pkgname=kirigami-addons
pkgver=0.11.75
pkgrel=0
pkgdesc="Add-ons for the Kirigami framework"
url="https://invent.kde.org/libraries/kirigami-addons"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="LGPL-2.0-or-later AND GPL-2.0-or-later"
depends="
	kirigami
	qt6-qtmultimedia
	"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kirigami-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/libraries/kirigami-addons.git"
source="https://download.kde.org/unstable/kirigami-addons/kirigami-addons-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# tst_sounds.qml is broken
	xvfb-run ctest --test-dir build --output-on-failure -E "tst_(sounds|avatar|album_(qmllist|abstractlist|qmlqobject)model).qml"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4fb5ce1a3aaeba7fc6092ca84adb919b1a3a8c9faf923e0ba81031ac667d8e6c3190bf46b1091b05e5d8ead81716037155e10810f1cf7573f9efd5bedb8b35b1  kirigami-addons-0.11.75.tar.xz
"
