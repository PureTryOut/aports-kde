# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kitemmodels
pkgver=5.249.0
pkgrel=0
pkgdesc="Models for Qt Model/View system"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-only AND LGPL-2.0-or-later"
depends_dev="qt6-qtbase-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt6-qtdeclarative-dev
	qt6-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-doc"
_repo_url="https://invent.kde.org/frameworks/kitemmodels.git"
source="https://download.kde.org/unstable/frameworks/$pkgver/kitemmodels-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# kdescendantsproxymodel_smoketest and kdescendantsproxymodeltest are broken
	# # kselectionproxymodeltest
	xvfb-run ctest --test-dir build --output-on-failure -E "(kselectionproxymodel|kdescendantsproxymodel(_smoke|))test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
06ec8b9860b133ab21d7b99aea6210db419b68bab262fccb51f244066a318b1a6ea0b11918b41e494091cdd068f1a32bb7cafe265dd427fd9a03df9c99266fce  kitemmodels-5.249.0.tar.xz
"
