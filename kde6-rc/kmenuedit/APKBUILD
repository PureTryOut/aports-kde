# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kmenuedit
pkgver=5.93.0
pkgrel=0
pkgdesc="KDE menu editor"
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemviews-dev
	kxmlgui-dev
	qt6-qtbase-dev
	samurai
	sonnet-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/plasma/kmenuedit.git"
source="https://download.kde.org/unstable/plasma/$pkgver/kmenuedit-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9e43a3aca35e8489940e371bf60368a2938cccf65dfd7f5db89bcbe087c11e6945056249e6678a0c72b89e8dd3c828d0b60f48877ceab3f98a46aa2ce3a991ec  kmenuedit-5.93.0.tar.xz
"
