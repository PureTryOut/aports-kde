# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=knetwalk
pkgver=24.01.95
pkgrel=0
pkgdesc="Connect all the terminals to the server, in as few turns as possible"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/knetwalk/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/games/knetwalk.git"
source="https://download.kde.org/unstable/release-service/$pkgver/src/knetwalk-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c862bbd16682c1b54598e176f15666495546336d8f4da749be260b4de3a3b59e4a1193244c33198fa70548b37b7591ae4cc4b52dfa6be7697077913de10a6b88  knetwalk-24.01.95.tar.xz
"
