# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kruler
pkgver=24.01.95
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/graphics/org.kde.kruler"
pkgdesc="An on-screen ruler for measuring pixels"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdoctools-dev
	ki18n-dev
	knotifications-dev
	kstatusnotifieritem-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt6-qtbase-dev
	samurai
	"
subpackages=" $pkgname-lang"
_repo_url="https://invent.kde.org/graphics/kruler.git"
source="https://download.kde.org/unstable/release-service/$pkgver/src/kruler-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
722cd29fd0990c796e36d869d576e307a1cbbcb601a45d16e19298eaf32ea23867985a93143be268aa4bcc4315c7bcc5422c1dee7eac4c33b7bdda07259ae697  kruler-24.01.95.tar.xz
"
