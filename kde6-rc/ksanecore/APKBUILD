# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ksanecore
pkgver=24.01.95
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/graphics/"
pkgdesc="Library providing logic to interface scanners"
license="BSD-2-Clause AND BSD-3-Clause AND CC0-1.0 AND (LGPL-2.1-only OR LGPL-3.0-only) AND LicenseRef-KDE-Accepted-LGPL"
depends_dev="
	ki18n-dev
	qt6-qtbase-dev
	sane-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/libraries/ksanecore.git"
source="https://download.kde.org/unstable/release-service/$pkgver/src/ksanecore-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cf542af0d2337c7f52874b3e80514c04ffe354dbc8febaa2c83e1bf2dbc18ef8a337f155f1a2c05882586b1cb6300e74d8bc6d457a2519b17b523c3c530f590c  ksanecore-24.01.95.tar.xz
"
