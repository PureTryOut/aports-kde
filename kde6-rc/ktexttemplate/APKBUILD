# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=ktexttemplate
pkgver=5.249.0
pkgrel=0
pkgdesc="Library to allow application developers to separate the structure of documents from the data they contain"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND LGPL-2.1-or-later"
makedepends="
	doxygen
	extra-cmake-modules
	qt6-qtbase-dev
	samurai
	graphviz
	qt6-qttools-dev
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-doc"
_repo_url="https://invent.kde.org/frameworks/ktexttemplate.git"
source="https://download.kde.org/unstable/frameworks/$pkgver/ktexttemplate-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
c8da68aaab195af66c9be9ac6db2cc31899fc13aae92775783286cde17db8daddabedd8064371934d722945842e9835c56fc333aa29860266bb3d5741d084b99  ktexttemplate-5.249.0.tar.xz
"
