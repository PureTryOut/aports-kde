# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ktnef
pkgver=24.01.95
pkgrel=0
pkgdesc="API for handling TNEF data"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="
	kcalendarcore-dev
	kcalutils-dev
	kcontacts-dev
	ki18n-dev
	qt6-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/pim/ktnef.git"
source="https://download.kde.org/unstable/release-service/$pkgver/src/ktnef-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
f0e9042138c0c68f5ff7fa0aef383a9cdee6a6eb887bbb073b7b006e63b042463ca65e1b06ad4e44399c55dfec8741b9b4db8ca03ee68192672b21d792efbbc1  ktnef-24.01.95.tar.xz
"
