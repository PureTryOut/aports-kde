# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma5support
pkgver=5.93.0
pkgrel=0
pkgdesc="Support components for porting from KF5/Qt5 to KF6/Qt6"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	doxygen
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	kpackage-dev
	kservice-dev
	kxmlgui-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qttools-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/plasma/plasma5support.git"
source="https://download.kde.org/unstable/plasma/$pkgver/plasma5support-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=ON \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
2d5fde7b3964fd5273b93d4df69ad1d6c73bc69e1878bc4980c3c8ad34c2de39b55b764b812dd161e67942f5f839bbf9b90847b664ac8463b84a8b45a79187b9  plasma5support-5.93.0.tar.xz
"
