# Contributor: jane400 <ralfrachinger@gmail.com>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=plasmatube
pkgver=24.01.95
pkgrel=0
pkgdesc="Kirigami YouTube video player based on Invidious"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/plasma-mobile/plasmatube"
license="GPL-3.0-or-later AND CC0-1.0"
depends="
	gst-libav
	gst-plugins-good
	gst-plugins-good-qt
	kcoreaddons
	kdeclarative
	kirigami
	qt6-qtimageformats
	yt-dlp
	"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami-dev	
	mpvqt-dev
	qt6-qtbase-dev
	qtkeychain-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/multimedia/plasmatube.git"
source="https://download.kde.org/unstable/release-service/$pkgver/src/plasmatube-$pkgver.tar.xz"
# No tests
options="!check"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cc1e8527f45c3f367fa95d91b66ae42aacda40e236833c70772a2a9c4b782effc6a0973bb142e2ebfb45faba5957db8ad4dc530ba11fb5be5ef4d0639ff5434e  plasmatube-24.01.95.tar.xz
"
