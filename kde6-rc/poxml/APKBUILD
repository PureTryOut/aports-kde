# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=poxml
pkgver=24.01.95
pkgrel=0
arch="all !armhf !riscv64"
url="https://www.kde.org/applications/development/"
pkgdesc="Translates DocBook XML files using gettext po files"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	gettext-dev
	kdoctools5-dev
	qt5-qtbase-dev
	samurai
	"
subpackages="$pkgname-doc"
_repo_url="https://invent.kde.org/sdk/poxml.git"
source="https://download.kde.org/unstable/release-service/$pkgver/src/poxml-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c9a14f6aae3ad811c9efabc642fb261b03c395002e90794371b1b0e2105e2fb5b9db4d7582f3383d293e9498c9e3c931725bef3169aac390c94bf5d57a55e5e1  poxml-24.01.95.tar.xz
"
