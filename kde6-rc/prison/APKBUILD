# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=prison
pkgver=5.249.0
pkgrel=0
pkgdesc="A barcode API to produce QRCode barcodes and DataMatrix barcodes"
arch="all !armhf" # armhf blocked by qt6-qtdeclarative
url="https://community.kde.org/Frameworks"
license="MIT"
depends_dev="
	libdmtx-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qtmultimedia-dev
	zxing-cpp-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	libqrencode-dev
	qt6-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc"
_repo_url="https://invent.kde.org/frameworks/prison.git"
source="https://download.kde.org/unstable/frameworks/$pkgver/prison-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# prison-datamatrixtest and prison-qrtest are broken on s390x
	case "$CARCH" in
		s390x) ctest --test-dir build --output-on-failure -E "prison-(datamatrix|qr)test" ;;
		*) ctest --test-dir build --output-on-failure ;;
	esac
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
17446bcf29b83f7fc8ee6c58a028c7968d37f62ac1a4f06870a352988faa5694b171c40bbfa30de029f6a6b308e9839a4f55c47864385d623875937c3ef3937e  prison-5.249.0.tar.xz
"
