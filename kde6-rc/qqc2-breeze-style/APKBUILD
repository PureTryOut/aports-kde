# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=qqc2-breeze-style
pkgver=5.93.0
pkgrel=0
pkgdesc="Breeze inspired QQC2 style"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.plasma-mobile.org/"
license="LicenseRef-KDE-Accepted-LGPL AND LicenseRef-KFQF-Accepted-GPL"
depends="kirigami"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kguiaddons-dev
	kiconthemes-dev
	kirigami-dev
	kquickcharts-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-dev"
_repo_url="https://invent.kde.org/plasma/qqc2-breeze-style.git"
source="https://download.kde.org/unstable/plasma/$pkgver/qqc2-breeze-style-$pkgver.tar.xz"

build() {
	cmake -B build -DBUILD_WITH_QT6=ON -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
792955759078c907a1084c35fc860f7862b25c15cbd3874cf9ff2948196ce0cebb7686a4019784b45ec4eb9c2d56dfd67d58834ab0c382fd6ea19dea45397157  qqc2-breeze-style-5.93.0.tar.xz
"
