#!/bin/bash

set -e

packages=($(git -C kde6-alpha grep -l group=kde-frameworks | cut -d '/' -f 1))
packages+=($(git -C kde6-alpha grep -l group=kde-plasma | cut -d '/' -f 1))
packages+=($(git -C kde6-alpha grep -l group=kde-applications | cut -d '/' -f 1))
packages+=($(git -C kde6-alpha grep -l group=kde-other | cut -d '/' -f 1))

packages=("${packages[@]/kde6-alpha\//}")

update_package() {
	apkbuild=$1
	pkgname=$2
	gitlab_name="$pkgname"

	if [ "$pkgname" == kdeconnect ]; then
		gitlab_name="kdeconnect-kde"
	elif [ "$pkgname" == index-fm ]; then
		gitlab_name="index"
	elif [ "$pkgname" == phonon-backend-vlc ]; then
		gitlab_name="phonon-vlc"
	elif [ "$pkgname" == polkit-qt ]; then
		gitlab_name="polkit-qt-1"
	fi

	if grep 'group=kde-frameworks' "$apkbuild" > /dev/null 2>&1; then
		pkgver=5.245.0
		sed -i "s|^pkgver=.*|pkgver=$pkgver|" "$apkbuild" > /dev/null 2>&1
		sed -i "s|^source=.*|source=\"https://download.kde.org/unstable/frameworks/\$pkgver/$gitlab_name-\$pkgver.tar.xz\"|" "$apkbuild" > /dev/null 2>&1
	elif grep 'group=kde-plasma' "$apkbuild" > /dev/null 2>&1; then
		pkgver=5.27.80
		sed -i "s|^pkgver=.*|pkgver=$pkgver|" "$apkbuild" > /dev/null 2>&1
		if grep 'source=' "$apkbuild" | grep '"$' > /dev/null 2>&1; then
			sed -i "s|^source=.*|source=\"https://download.kde.org/unstable/plasma/\$pkgver/$gitlab_name-\$pkgver.tar.xz\"|" "$apkbuild" > /dev/null 2>&1
		else
			sed -i "s|^source=.*|source=\"https://download.kde.org/unstable/plasma/\$pkgver/$gitlab_name-\$pkgver.tar.xz|" "$apkbuild" > /dev/null 2>&1
		fi
	elif grep 'group=kde-applications' "$apkbuild" > /dev/null 2>&1; then
		pkgver=24.01.75
		sed -i "s|^pkgver=.*|pkgver=$pkgver|" "$apkbuild" > /dev/null 2>&1

		if grep 'source=' "$apkbuild" | grep '"$' > /dev/null 2>&1; then
			sed -i "s|^source=.*|source=\"https://download.kde.org/unstable/release-service/\$pkgver/src/$gitlab_name-\$pkgver.tar.xz\"|" "$apkbuild" > /dev/null 2>&1
		else
			sed -i "s|^source=.*|source=\"https://download.kde.org/unstable/release-service/\$pkgver/src/$gitlab_name-\$pkgver.tar.xz|" "$apkbuild" > /dev/null 2>&1
		fi
	elif grep 'group=kde-other' "$apkbuild" > /dev/null 2>&1; then
		if [ "$pkgname" == haruna ]; then
			pkgver=0.12.2
			sed -i "s|^source=.*|source=\"https://download.kde.org/stable/haruna/$gitlab_name-\$pkgver.tar.xz\"|" "$apkbuild" > /dev/null 2>&1
		elif [ "$pkgname" == ktextaddons ]; then
			pkgver=1.5.2
			sed -i "s|^source=.*|source=\"https://download.kde.org/stable/ktextaddons/$gitlab_name-\$pkgver.tar.xz\"|" "$apkbuild" > /dev/null 2>&1
		elif [ "$pkgname" == kirigami-addons ]; then
			pkgver=0.11.75
			sed -i "s|^source=.*|source=\"https://download.kde.org/unstable/kirigami-addons/$gitlab_name-\$pkgver.tar.xz\"|" "$apkbuild" > /dev/null 2>&1
		elif [ "$pkgname" == libqaccessibilityclient ]; then
			pkgver=0.5.0
			sed -i "s|^source=.*|source=\"https://download.kde.org/stable/libqaccessibilityclient/$gitlab_name-\$pkgver.tar.xz\"|" "$apkbuild" > /dev/null 2>&1
		elif [ "$pkgname" == phonon-backend-vlc ]; then
			pkgver=0.12.0
			sed -i "s|^source=.*|source=\"https://download.kde.org/stable/phonon/phonon-backend-vlc/\$pkgver/phonon-backend-vlc-\$pkgver.tar.xz\"|" "$apkbuild" > /dev/null 2>&1
		elif [ "$pkgname" == phonon ]; then
			pkgver=4.12.0
			sed -i "s|^source=.*|source=\"https://download.kde.org/stable/phonon/$pkgver/$gitlab_name-\$pkgver.tar.xz\"|" "$apkbuild" > /dev/null 2>&1
		elif [ "$pkgname" == plasma-wayland-protocols ]; then
			pkgver=1.11.0
			sed -i "s|^source=.*|source=\"https://download.kde.org/stable/plasma-wayland-protocols/$gitlab_name-\$pkgver.tar.xz\"|" "$apkbuild" > /dev/null 2>&1
		elif [ "$pkgname" == polkit-qt ]; then
			pkgver=0.175.0
			sed -i "s|^source=.*|source=\"https://download.kde.org/unstable/polkit-qt-1/$gitlab_name-\$pkgver.tar.xz\"|" "$apkbuild" > /dev/null 2>&1
		fi

		sed -i "s|^pkgver=.*|pkgver=$pkgver|" "$apkbuild" > /dev/null 2>&1
	fi

	sed -i "s|pkgrel=.*|pkgrel=0|" "$apkbuild" > /dev/null 2>&1
	sed -i "s|^options=\"!check\"||" "$apkbuild" > /dev/null 2>&1
	sed -i "s|^builddir=.*||" "$apkbuild" > /dev/null 2>&1
	sed -i "s|^_commit=.*||" "$apkbuild" > /dev/null 2>&1	

	pushd "kde6-alpha/$pkgname" > /dev/null 2>&1
	abuild checksum > /dev/null 2>&1
	popd > /dev/null 2>&1
}

echo "\n"
for pkgname in "${packages[@]}"; do
	apkbuild="kde6-alpha/$pkgname/APKBUILD"
	echo -e "\e[1A\e[KPreparing $pkgname"

	gitlab_name=$pkgname
	if [ "$pkgname" == kirigami2 ]; then
		gitlab_name=kirigami
	elif [ "$pkgname" == oxygen-icons ]; then
		gitlab_name=oxygen-icons5
	elif [ "$pkgname" == kdeconnect ]; then
		gitlab_name=kdeconnect-kde
	elif [ "$pkgname" == index ]; then
		gitlab_name=index-fm
	elif [ "$pkgname" == phonon-backend-vlc ]; then
		gitlab_name=phonon-vlc
	elif [ "$pkgname" == polkit-qt ]; then
		gitlab_name=polkit-qt-1
	fi

	update_package "$apkbuild" "$gitlab_name"
done
